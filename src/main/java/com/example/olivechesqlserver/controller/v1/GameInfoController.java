package com.example.olivechesqlserver.controller.v1;

import com.example.olivechesqlserver.model.InMessage;
import com.example.olivechesqlserver.model.OutMessage;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;


@Controller
public class GameInfoController {

	
	@MessageMapping("/v1/chat")
	@SendTo("/topic/game_chat")
	public OutMessage gameInfo(InMessage message){
		return new OutMessage(message.getContent());
	}

	
	

}



