package com.example.olivechesqlserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.example.olivechesqlserver.Mapper")
@EnableScheduling
public class OlivecheSqlserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(OlivecheSqlserverApplication.class, args);
    }

}
