package com.example.olivechesqlserver.controller;


import com.example.olivechesqlserver.Mapper.KeepDataMapper;
import com.example.olivechesqlserver.service.WebSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/v1/pub/oliveche")
public class keepdatecontroller {

    @Autowired
    private WebSocketService webSocketService;

    @Autowired
    private KeepDataMapper keepDataMapper;


    @RequestMapping("KeepData")
    public void update(@RequestParam(value = "table") String table,@RequestParam(value = "temp") int temp,@RequestParam(value = "id") String id, HttpServletRequest request) {

        if (table.equals("check")){
            try {
                keepDataMapper.updateHum(temp,id);
            } catch (Exception e) {
                webSocketService.sendDatebaseMessage(e.toString());
                e.printStackTrace();
            }
        }else if(table.equals("trigger")){
            try {
                keepDataMapper.updateHum1(temp,id);
            } catch (Exception e) {
                webSocketService.sendDatebaseMessage(e.toString());
                e.printStackTrace();
            }
        }



    }

}
