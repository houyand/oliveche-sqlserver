package com.example.olivechesqlserver.Mapper;

import org.apache.ibatis.annotations.Param;

public interface KeepDataMapper {
    /**
     * check约束
     * 更新 仓储.仓库温度
     * @param hum
     */
    void updateHum(@Param("hum") int hum,@Param("id")String id);

    /**
     * trigger
     * 触发器
     * @param hum
     */
    void updateHum1(@Param("hum") int hum,@Param("id")String id);

}
